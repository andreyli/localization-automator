# Localization Automator

This repository contains the source code for localization automation tool

## Development Environment

### Software Requirements

* [XCode](https://developer.apple.com/xcode/downloads/)
* [Homebrew](http://brew.sh/)
* [PIP](http://pip.pypa.io)

### Dependencies

**Homebrew**

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

**PIP**

```
$ curl -O https://bootstrap.pypa.io/get-pip.py
$ sudo python get-pip.py
```

**System Packages**

```
$ xcode-select --install
$ brew update
```

### Workspace

**Python Modules**

```
$ sudo pip install XlsxWriter pyyaml

```

### Configuring script

Use config.yaml to configure script. Current supported configurations:

* HMI project repo.
* Layout and sting values paths.
* Excel spreadsheet schema.
* List of string ids to parse.

### Running script

```
$ python localization-automator.py

```

### Generated spreadsheet

Generated spreadsheed is located at /out