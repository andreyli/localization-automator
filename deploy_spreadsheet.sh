#!/usr/bin/env bash

# This script deploys excel spreadsheet to git repo
usage() {
    echo "Usage: $(basename "$0") -o <outPath>"
}

error() {
    echo "Error: Invalid arguments"
    usage
    exit 1
}

if [ $# -lt 1 ]; then
    usage
    exit
fi

while [ $# -gt 1 ]; do
    case "$1" in
        -o)
            out_path="$2"
            shift;;
        *)
            error
            ;;
    esac
    shift
done

git_repo_dir="master-strings"

init() {
    if [ -d "$out_path" ]; then
        cd "$out_path"
        
        if [ -d "$git_repo_dir" ]; then
            # Pull latest changes
            git pull origin master
        else
            # Clone repo
            git clone "https://andreyli@bitbucket.org/andreyli/${git_repo_dir}.git"
        fi
    else
        echo "Output directory $out_path doesn't exist"
        exit 1
    fi
}

unzip() {
    echo "Uncompressing spreadsheet: make sure spreadsheet is closed..."
    sudo unzip -o *.xlsx -d "${git_repo_dir}/"
}

commit() {
    cd "$git_repo_dir"
    git add --all
    git commit -m "Updating master strings"
    git push origin master:master
}

init
unzip
commit