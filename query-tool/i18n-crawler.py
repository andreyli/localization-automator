#!/usr/bin/env python

import argparse
import sqlite3
import sys
import os
import xml.etree.ElementTree as elemTree
import yaml
import pdb
import csv

reload(sys)
sys.setdefaultencoding('utf-8')

class LocalizationCrawler():
    # Constants
    VALUES_FOLDER_PATH = "values"

    # Config file handle
    config = None

    # Output directory path
    output_dir = ""

    conn = None

    def __init__(self, output_dir, config):
        self.output_dir = output_dir
        self.config = config

    def __del__(self):
        if not (self.conn is None): 
            self.conn.close()

    def createTable(self):
        self._connectIfNot()

        cursor = self.conn.cursor()
        
        # Create table
        cursor.execute('''CREATE TABLE strings(stringId text, locale text, value blob)''')

        # Crawl all strings and populate database
        for path in self._getResourcePaths(self.config):
            self._fetchStringValues(path, cursor)
        self.conn.commit()

    def _fetchStringValues(self, path, cursor):
        """Fetch string values from xml file(s)

        Note: there might be multiple files to string values

        Args:
            path: Path to the project values folder
            cursor: databse cursor

        """
        for directory in os.listdir(path):
            if not directory.startswith(self.VALUES_FOLDER_PATH): continue
        
            for fileName in os.listdir(os.path.join(path, directory)):
                if not fileName.endswith('.xml'): continue

                fullName = os.path.join(os.path.join(path, directory), fileName)
                tree = elemTree.parse(fullName)

                for elem in tree.iter(tag = 'string'):
                    stringId = elem.attrib['name']
                    locale = self._getLocale(directory)

                    if elem.text is None: continue
                    cursor.execute('INSERT INTO strings VALUES (?,?,?)', (stringId, locale, elem.text))

    def _getLocale(self, directory):
        language = directory.split('-')
        locale = 'EN-US'
        if len(language) > 1:  
            locale = language[1].upper()
        
        if len(language) > 2: 
            locale += '-' + language[2]

        return locale

    def _getResourcePaths(self, config):
        return list(map(lambda x: "{dir}/{res}".format(dir=self.output_dir, res=x), config['ResourcePath']))

    def query(self, stringIds, locale):
        self._connectIfNot()

        cursor = self.conn.cursor()
        if not locale:
           cursor.execute('SELECT * FROM strings  WHERE stringId=?', (stringIds,))
        elif not stringIds: 
           cursor.execute('SELECT * FROM strings WHERE locale=?', (locale,))
        else:
           cursor.execute('SELECT * FROM strings WHERE stringId=? AND locale=?', (stringIds, locale)) 
        
        for row in cursor.fetchall():
            print "STRING_ID: {stringID} | LOCALE: {locale} | TEXT: {text}".format(stringID=row[0], locale=row[1], text=row[2])

    def _connectIfNot(self):
        if self.conn is None:
           self.conn = sqlite3.connect("{dir}/{db_file}".format(dir=self.output_dir, db_file='strings.db'))

    def exportTo(self, fileName):
        file = open(self.output_dir + "/" + fileName, "w")
        writer = csv.writer(file, delimiter=",")
        writer.writerow(["STRING_ID", "LOCALE", "TEXT"])

        self._connectIfNot()
        cursor = self.conn.cursor()
        cursor.execute('SELECT * FROM strings')

        for row in cursor.fetchall():
            writer.writerow([row[0], row[1], row[2]])
 
def getArgumentParser(argv):
    """Initialize and return argument parser

        Args:
            argv: arguments

        Returns:
            handle to argument parser

        """
    argument_parser = argparse.ArgumentParser(
                                 description='This script crawls string translations')
    argument_parser.add_argument('command', nargs='?', default=None, help='Action to perform')
    argument_parser.add_argument('--stringId', default=None, help='List of string ids')
    argument_parser.add_argument('--locale', default=None, help='List of locales')
    argument_parser.add_argument('--config_path', default='config.yaml',
                                 help='Configure paths to project and resources')
    argument_parser.add_argument('--output_dir', default='out',
                                 help='Output workspace directory')
    argument_parser.add_argument('--output_file', default='string.csv',
                                 help='File name for exported data')

    return argument_parser

def fetchResourceFiles(git_repo, resource_paths, branch, out_path):
    """Fetch sparse checkout of the project git repo

    Args:
        git_repo: Path to the project git repo
        resource_path: Path to resource folder
        out_path: Default path to output directory

    """
    print "Fetching resource files, this could take some time..."
    # Resources might be stored in multiple folders
    os.system("./fetch_resources.sh -g {repo} -o {path} -b {branch} -p {res}".format(repo=git_repo, res=' '.join(resource_paths), path=out_path, branch=branch))

def readConfigFile(path):
        """Read yaml config file

        Args:
            path: Path to yaml config file

        Returns: 
            Handle to config file

        """
        try:
            file = open(path)
            config = yaml.load(file)
        except yaml.YAMLError as exception:
            print exception
        finally:
            file.close()
        return config

def main(argv):
    # Uncomment to start debugging
    # pdb.set_trace()

    # Parse input arguments 
    argument_parser = getArgumentParser(argv)
    arguments = argument_parser.parse_args()
    config = readConfigFile(arguments.config_path)

    if arguments.command is None:
        print 'Nothing to do'
        return

    # Create crawler
    crawler = LocalizationCrawler(arguments.output_dir, config)
    
    # Handle update command
    if arguments.command == 'update':
        fetchResourceFiles(config['GitRepository'], config['ResourcePath'], config['Branch'], arguments.output_dir)
        crawler.createTable()
        return
    
    # Handler query command
    if arguments.command == 'query':
        if arguments.locale is None and arguments.stringId is None:
            print 'Insufficient data to preform a query: provide string id or locale'
            return

        crawler.query(arguments.stringId, arguments.locale)

    # Handle export command
    elif arguments.command == 'export':
        crawler.exportTo(arguments.output_file)
    else:
        print 'Unsupported command'

if __name__ == "__main__":
    main(sys.argv)