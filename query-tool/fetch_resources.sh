#!/usr/bin/env bash

# This script does sparse checkout of resource folder of specific HMI project. 
usage() {
    echo "Usage: $(basename "$0") -g <gitRepo> -p <pathToResource> -o <outPath>"
}

error() {
    echo "Error: Invalid arguments"
    usage
    exit 1
}

if [ $# -lt 4 ]; then
    usage
    exit
fi

while [ $# -gt 1 ]; do
    case "$1" in
        -g)
            git_repo="$2"
            shift;;
        -p)
            shift
            resouces=("$@")
            shift;;
        -o)
            out_path="$2"
            shift;;
        -b)
            branch="$2"
            shift;;
        *)
            error
            ;;
    esac
    shift
done

init() {
    if [ -d "$out_path" ]; then
        echo "Cleaning $out_path"
        rm -rf "$out_path"
    fi
    mkdir "$out_path" && cd "$out_path"
}

git_checkout() {
    git init
    git remote add -f origin "$git_repo"
    git config core.sparseCheckout true
    for resource in "${resouces[@]}"
        do
            echo "Sparse checkout: $resource"
            echo "$resource" >> .git/info/sparse-checkout
        done
    git pull origin "$branch"
    cd ..
}

init
git_checkout