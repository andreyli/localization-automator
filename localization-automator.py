#!/usr/bin/env python

import argparse
import sys
import os
import xlsxwriter
import xml.etree.ElementTree as elemTree
import yaml
import pdb

class LocalizationAutomator():
    """Localization Automator generates Excel spreadsheet for string localizations

    This script generates Excel spreadsheet with all strings and complimentary
    data that is captured by processing resource files for a particular HMI
    project.

    Args:
        config_path (str): Path to yaml configuration file
        output_dir (str): Path to output directory, used to keep transient data

    """
    # Constants 
    STATIC = "static"
    STYLES_FILE = "values/styles.xml"
    LAYOUT_FOLDER_PATH = "layout"
    VALUES_FOLDER_PATH = "values"

    COLUMN_WIDTH = { "string_id": 25, 
                     "string_type": 15,  
                     "screen_id": 30,
                     "dpi": 15,
                     "region_code": 15,
                     "dnt": 15,
                     "line_text": 40,  
                     "font_size": 15, 
                     "font_name": 25,
                     "plurals": 15 }

    # Config file handle
    config = None

    # Excel workbook handle
    workbook = None

    # Output directory path
    output_dir = ""

    # Set of transient dictionaries
    stringValuesDictionary = {}    # [stringId: text]
    stringLayoutDictionary = {}    # [layoutName: stringId]
    dimensionLayoutDictionary = {} # [stringId: dimensionName] 
    stringIdStyleDictionary = {}   # [stringId: styleName]
    styleFontNameDictionary = {}   # [styleName: fontName]
    dimensionSizeDictionary = {}   # [dimensionName: dp/sp]

    # Record formats
    longTextFormat = None
    defaultFormat = None

    def __init__(self, config_path, output_dir, clean):
        self.config = self._readConfigFile(config_path)
        self.output_dir = output_dir

        if not os.path.isdir(output_dir) or clean == 'true':
            self._fetchResourceFiles(self.config['GitRepository'], self.config['ResourcePath'], output_dir)

    def __del__(self):
        if not (self.workbook is None): 
            self.workbook.close()
            # MacOS specific command
            os.system("open {}/*.xlsx".format(self.output_dir))

    def getResourcePaths(self):
        return list(map(lambda x: "{dir}/{res}".format(dir=self.output_dir, res=x), self.config['ResourcePath']))

    def fetchStringIds(self):
        """Fetch and process all resource xml files
        
        Returns:
            List of valid string ids found in layout files

        """
        # TODO: take in to account list of ids.
        for path in self.getResourcePaths():
            self.stringValuesDictionary.update(self._fetchStringValues(path + self.VALUES_FOLDER_PATH))

            stringLayoutDictionary, dimensionLayoutDictionary, stringIdStyleDictionary = self._findAllLayoutsAndSizeOfStringIDs(path + self.LAYOUT_FOLDER_PATH, self.stringValuesDictionary)
            self.stringLayoutDictionary.update(stringLayoutDictionary)
            self.dimensionLayoutDictionary.update(dimensionLayoutDictionary)
            self.stringIdStyleDictionary.update(stringIdStyleDictionary)

        print("Strings in total: {}".format(len(self.stringValuesDictionary.keys())))
        mapped = set(self.stringLayoutDictionary.keys()) & set(self.stringValuesDictionary.keys())
        print("Strings mapped: {}".format(len(mapped)))
        return mapped

    def fetchFontNames(self, string_ids):
        for path in self.getResourcePaths():
            self.styleFontNameDictionary.update(self._fetchFontNames(path + self.STYLES_FILE))
        filtered_styles = { k:v for k,v in self.stringIdStyleDictionary.items() if k in string_ids and v in self.styleFontNameDictionary}
        return list(map(lambda x: self.styleFontNameDictionary[x], filtered_styles.values()))

    def _toPx(self, dpi, dp):
        dp = dp[:-2]
        return int(dp) / (160 / dpi)

    def fetchFontSizes(self, dpi, string_ids):
        for path in self.getResourcePaths():
            self.dimensionSizeDictionary.update(self._findSizeOfAllDimensions(path + self.VALUES_FOLDER_PATH, dpi))

        filtered_dimensions = { k:v for k,v in self.dimensionLayoutDictionary.items() if k in string_ids }
        font_sizes_sp = list(map(lambda x: self.dimensionSizeDictionary[x], filtered_dimensions.values()))
        return list(map(lambda x: "{}px".format(self._toPx(dpi, x)), font_sizes_sp))

    def _toCamelCase(self, snake_case):
        components = snake_case.split('_')
        return "".join(x.title() for x in components[0:])

    def _formatScreenIds(self, ids_list):
        # First convert snake case titles to camel case
        formatted_list = list(map(lambda x: self._toCamelCase(x), ids_list))
        # Reduce list to a single title string
        return reduce((lambda x, y: "{}, {}".format(x,y)), formatted_list)

    def fetchScreenIds(self, string_ids):
        """Fetch screen ids in specific format
        
        Args:
            string_ids: set of unique string ids

        Returns:
            List of screen ids: [MapSettings, Search]

        """
        filtered_dict = { k:v for k,v in self.stringLayoutDictionary.items() if k in string_ids }
        return list(map(lambda x: self._formatScreenIds(x), filtered_dict.values()))

    def fetchStringValues(self, string_ids):
        """Fetch string values
        
        Args:
            string_ids: set of unique string ids

        Returns:
            List of string values

        """
        filtered_dict = { k:v for k,v in self.stringValuesDictionary.items() if k in string_ids }
        return filtered_dict.values()

    def createWorkSheet(self, output_dir, output_file):
        """Create Excel workbook and worksheet

        Args:
            output_dir: Path to output directory
            output_file: Output file name, should strictly be "foo.xlsx"
        
        Returns:
            Handle to Excel worksheet

        """
        print "Creating Excel spreadsheet according to schema..." 
        self.workbook = xlsxwriter.Workbook("{dir}/{filename}".format(dir=output_dir, filename=output_file))
        
        # TODO: Expose worksheet name to config
        sheet = self.workbook.add_worksheet("Master Strings")

        # Freeze top row
        sheet.freeze_panes(1, 0)

        # Create header format
        header = self.workbook.add_format({'bold': True, 'bg_color': '#A9A9A9'})
        header.set_align('center')

        # Set default column width
        sheet.set_column('A:AA', 20)

        # Create long text format
        self.longTextFormat = self.workbook.add_format()
        self.longTextFormat.set_text_wrap()

        # Create default row format
        self.defaultFormat = self.workbook.add_format()
        self.defaultFormat.set_align('center')

        return self.writeRow(sheet, 0, self.config['Schema'], header)

    def _indexOf(self, rowId):
        return self.config['Schema'].index(rowId)

    def _getFontSize(self, stringId, dpi):
        try:
            dimension = self.dimensionLayoutDictionary[stringId]
        except KeyError:
            return ""

        try:
            font_size = self.dimensionSizeDictionary[dimension]
        except KeyError:
            return ""

        return "{}px".format(self._toPx(dpi, font_size))

    def _getFontName(self, stringId):
        try:
            style = self.stringIdStyleDictionary[stringId]
        except KeyError:
            return ""
        try:
            return self.styleFontNameDictionary[style]
        except KeyError:
            return ""

    def _setColumnWidth(self, sheet, width_dict):
        for tag, width in width_dict.items():
            sheet.set_column(self.indexToTitleRange(self._indexOf(tag) + 1), width)

    def writeData(self, sheet, string_ids, dpi):
        self._setColumnWidth(sheet, self.COLUMN_WIDTH)
        count = 1
        for stringId in string_ids:
            sheet.write(count, self._indexOf("string_id"), stringId, self.longTextFormat)
            sheet.write(count, self._indexOf("string_type"), "static", self.defaultFormat)
            sheet.write(count, self._indexOf("screen_id"), self._formatScreenIds(self.stringLayoutDictionary[stringId]), self.longTextFormat)
            sheet.write(count, self._indexOf("dpi"), dpi, self.defaultFormat)
            sheet.write(count, self._indexOf("line_text"), self.stringValuesDictionary[stringId], self.longTextFormat)
            sheet.write(count, self._indexOf("multiline"), 1, self.defaultFormat)
            sheet.write(count, self._indexOf("region_code"), "GLOBAL", self.defaultFormat)
            sheet.write(count, self._indexOf("dnt"), "FALSE", self.defaultFormat)
            sheet.write(count, self._indexOf("font_size"), self._getFontSize(stringId, dpi), self.defaultFormat)
            sheet.write(count, self._indexOf("font_name"), self._getFontName(stringId), self.defaultFormat)
            sheet.write(count, self._indexOf("plurals"), "FALSE", self.defaultFormat)
            count += 1

    def writeConstColumn(self, sheet, col, const, max):
        """Write a column of data to Excel worksheet

        Args:
            sheet: Handle to excel worksheet
            col: Column number from 0-N or A-...
            const: Constant to write
            max: Max number of records
            format: Data format, default is None

        Returns:
            Handle to Excel worksheet

        """
        return self.writeColumn(sheet, col, [const]*max, self.defaultFormat)

    def _convertToTitle(self, num):
        return "" if num == 0 else self._convertToTitle((num - 1) / 26) + chr((num - 1) % 26 + ord('A'))

    def indexToTitleRange(self, index):
        return "{N}:{N}".format(N=self._convertToTitle(index))

    def writeColumn(self, sheet, col, data, format = None, width = 15):
        """Write a column of data to Excel worksheet

        Args:
            sheet: Handle to excel worksheet
            col: Column number from 0-N or A-...
            data: List to write
            format: Data format, default is None
            width: Column width

        Returns:
            Handle to Excel worksheet

        """
        # Set column width
        sheet.set_column(self.indexToTitleRange(col + 1), width)

        # Row 0 is occipied by the header
        rowCount = 1    
        for item in data:
            if not (format is None):
                sheet.write(rowCount, col, item, format)
            else: 
                sheet.write(rowCount, col, item)
            rowCount += 1
        return sheet

    def writeRow(self, sheet, row, data, format = None):
        """Write a row of data to Excel worksheet

        Args:
            sheet: Handle to excel worksheet
            row: Row number from 0-N
            data: List to write
            format: Data format, default is None

        Returns:
            Handle to Excel worksheet

        """
        colCount = 0    
        for item in data:
            if not (format is None):
                sheet.write(row, colCount, item, format)
            else: 
                sheet.write(row, colCount, item)
            colCount += 1
        return sheet  

    def _readConfigFile(self, path):
        """Read yaml config file

        Args:
            path: Path to yaml config file

        Returns: 
            Handle to config file

        """
        print "Reading configuration file..."
        try:
            file = open(path)
            config = yaml.load(file)
        except yaml.YAMLError as exception:
            print exception
        finally:
            file.close()
        return config

    def _fetchResourceFiles(self, git_repo, resource_paths, out_path):
        """Fetch sparse checkout of the project git repo

        Args:
            git_repo: Path to the project git repo
            resource_path: Path to resource folder
            out_path: Default path to output directory

        """
        print "Fetching resource files, this could take some time..."
        # Resources might be stored in multiple folders
        os.system("./fetch_resources.sh -g {repo} -o {path} -p {res}".format(repo=git_repo, res=' '.join(resource_paths), path=out_path))

    def _fetchStringValues(self, path):
        """Fetch string values from xml file(s)

        Note: there might be multiple files to string values

        Args:
            path: Path to the project values folder

        """
        stringValuesDictionary = dict()
        for fileName in os.listdir(path):
            if not fileName.endswith('.xml'): continue

            fullName = os.path.join(path, fileName)
            tree = elemTree.parse(fullName)

            for elem in tree.iter(tag = 'string'):
                attribute = elem.attrib['name']
                stringValuesDictionary[attribute] = elem.text
        return stringValuesDictionary

    def _findAllLayoutsAndSizeOfStringIDs(self, layout_folder_path, string_values):
        """Map string ids and layout files, as well as layout files and dimensions

        Note: Ranier has dynamic font size, should come up with better approach for it

        Args:
            layout_folder_path: Path to the project layout files
            string_values: String values dictionary

        Returns:
            Dictionaries: [stringId: "Screen Name", ...], ["Screen Name" : Dimension, ...] 

        """
        stringLayoutDictionary = dict()
        dimensionLayoutDictionary = dict()
        stringIdStyleDictionary = dict()

        findString = '@string/'
        findDimension = '@dimen/'
        fontSize = '{http://schemas.android.com/apk/res/android}textSize'
        findStyle = '@style/'
        for fileName in os.listdir(layout_folder_path):
            if not fileName.endswith('.xml'): continue

            fullName = os.path.join(layout_folder_path, fileName)
            base, ext = os.path.splitext(fileName)
            tree = elemTree.parse(fullName)

            # Iterate over all Elements
            for element in tree.getiterator():
                stringId = None
                dimension = None
                style = None

                for key, value in element.attrib.iteritems():
                    # Finding font size of stringID
                    if fontSize in key and findDimension in value: 
                        dimension = value[value.index(findDimension) + len(findDimension):]

                    # Find layout of stringID   
                    if findString in value: 
                        stringId = value[value.index(findString) + len(findString):]

                    # Find style
                    if findStyle in value: 
                        style = value[value.index(findStyle) + len(findStyle):]

                if stringId is None: continue
                
                if not (style is None): stringIdStyleDictionary[stringId] = style
                if not (dimension is None): dimensionLayoutDictionary[stringId] = dimension
                if stringId in string_values:
                    try:
                        stringLayoutDictionary[stringId].append(base)
                    except KeyError:
                        stringLayoutDictionary[stringId] = [base]

        # Remove repeated layout values from the dictionary
        stringLayoutDictionary = { key: sorted(list(set(value))) for (key, value) in stringLayoutDictionary.iteritems() }
        return stringLayoutDictionary, dimensionLayoutDictionary, stringIdStyleDictionary

    def _findSizeOfAllDimensions(self, values_folder_path, dpi):
        """Fetch all dimension files and map tags and values

        Args:
            values_folder_path: Path to the project dimension files
            dpi: Resolution

        Returns:
            Dictionaries: [Dimension: Value, ...]

        """
        dimensionSizeDictionary = dict()
        for fileName in os.listdir(values_folder_path):
            if not fileName.endswith('.xml'): continue

            fullName = os.path.join(values_folder_path, fileName)
            tree = elemTree.parse(fullName)
            
            for element in tree.iter(tag = 'dimen'):
                attribute = element.attrib['name']
                dimensionSizeDictionary[attribute] = element.text
        return dimensionSizeDictionary

    def _fetchFontNames(self, styles_path):
        fontNamesDictionary = dict()
        tree = elemTree.parse(styles_path)

        for style in tree.iter(tag = 'style'):
            styleName = style.attrib['name']
            for item in style.iter(tag = 'item'):
                if not (item.attrib['name'] == "android:fontFamily"): continue
                # Map font name to style name
                fontNamesDictionary[styleName] = item.text
        return fontNamesDictionary

def getArgumentParser(argv):
    """Initialize and return argument parser

        Args:
            argv: arguments

        Returns:
            handle to argument parser

        """
    argument_parser = argparse.ArgumentParser(
                                 description='This script generates Excel workbook for string localization')
    argument_parser.add_argument('--clean', default='true',
                                 help='Configure schema and string ids')
    argument_parser.add_argument('--config_path', default='config.yaml',
                                 help='Configure schema and string ids')
    argument_parser.add_argument('--output_dir', default='out',
                                 help='Output directory for generated Excel spreadsheet')
    argument_parser.add_argument('--dpi', default=160,
                                 help='Default dpi setting')
    argument_parser.add_argument('--output_file', default='out.xlsx',
                                 help='Output file generated by this script.')

    return argument_parser


def main(argv):
    # Uncomment to start debugging
    # pdb.set_trace()

    # Parse input arguments 
    argument_parser = getArgumentParser(argv)
    arguments = argument_parser.parse_args()

    # Create automator
    automator = LocalizationAutomator(arguments.config_path, arguments.output_dir, arguments.clean)

    # Create Excel worksheet 
    sheet = automator.createWorkSheet(arguments.output_dir, arguments.output_file)

    # Fetch strings for localization
    stringIds = automator.fetchStringIds()
    
    automator.fetchScreenIds(stringIds)
    automator.fetchFontSizes(arguments.dpi, stringIds)
    automator.fetchFontNames(stringIds)
    
    automator.writeData(sheet, stringIds, arguments.dpi)

if __name__ == "__main__":
    main(sys.argv)